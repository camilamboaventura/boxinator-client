import { KeycloakService } from "keycloak-angular"

export function initializeKeycloak(
    keycloak: KeycloakService
): () => Promise<boolean> {
    return () =>
        keycloak.init({
            config: {
                url: "http://localhost:8080/auth",
                realm: "angular-web", //@TODO CHANGE TO REALM ID
                clientId: "angular-web-client", //@TODO CHANGE TO CLIENT ID
            },
            initOptions: {
                checkLoginIframe: true, // set iframe to check if user is logged in
                checkLoginIframeInterval: 25, // interval 25s
            },
        })
}
